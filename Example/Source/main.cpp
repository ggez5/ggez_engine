#include <Window/WindowContext.h>
#include <iostream>

int main()
{
    ggez::WindowSettings windowSettings{};
    ggez::WindowContext::InitializeWindowContext("test", 500, 500, windowSettings);
	
	std::cout << "Window Opened" << std::endl;
   	

    ggez::WindowContext::Terminate();
}
