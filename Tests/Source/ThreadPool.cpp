#include <algorithm>
#include <chrono>
#include <functional>
#include <mutex>
#include <thread>
#include <utility>
#include <vector>

#include <gtest/gtest.h>

#include <Runnable.h>
#include <WorkQueue.h>

class ThreadTest : public ggez::Runnable
{
public:
    ThreadTest()
    {}

    ThreadTest(const ThreadTest& other) :
        m_func(other.m_func)
    {}

    ThreadTest(ThreadTest&& other) noexcept :
        m_func(std::move(other.m_func))
    {}

    ThreadTest(std::function<void()> func) :
        m_func{func}
    {}

    ThreadTest& operator=(const ThreadTest& other)
    {
        m_func = other.m_func;
        return *this;
    }

    ThreadTest& operator=(ThreadTest&& other)
    {
        m_func = std::move(other.m_func);
        return *this;
    }

    ~ThreadTest() override
    {}

    void Run() override
    {
        m_func();
    }

private:
    std::function<void()> m_func;
};

class ThreadPool : public ::testing::Test
{
protected:
    ThreadPool()
    {}

    ~ThreadPool() override
    {}

    void SetUp() override
    {
        testItems = new ThreadTest[ggez::WorkQueue::GetThreadMax()];
    }

    void TearDown() override
    {
        delete[] testItems;
    }

    ThreadTest* testItems;
};


TEST_F(ThreadPool, MainThreadIsAlwaysIdZero)
{
    ASSERT_EQ(0, ggez::WorkQueue::GetThreadId());
}

TEST_F(ThreadPool, AllThreadsHaveValidIds)
{
    ggez::WorkQueue* queue = ggez::WorkQueue::Instance();
    ggez::uint32_t threadMax = ggez::WorkQueue::GetThreadMax();
    std::mutex vectorWaiter;
    std::vector<ggez::uint64_t> threadIds;

    for (ggez::uint32_t index = 0; threadMax > index; ++index) {
        auto func = [&vectorWaiter,&threadIds] () mutable {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            std::lock_guard<std::mutex> lock(vectorWaiter);
            threadIds.push_back(ggez::WorkQueue::GetThreadId());
        };
        *(testItems + index) = ThreadTest(func);
        queue->AddWorkItem(testItems + index);
    }

    for (; threadIds.size() != threadMax; )
    {}

    auto it = std::unique(threadIds.begin(), threadIds.end());

    // They should all have unique identifier values.
    ASSERT_EQ(threadIds.end(), it);
}

