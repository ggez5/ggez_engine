#if !defined(ENGINE_PLATFORM)
#define ENGINE_PLATFORM 1

namespace ggez
{

#define GGEZ_Join2(A, B) A ## B
#define GGEZ_Join(A, B) GGEZ_Join2(A, B)

struct GenericTypes
{
    typedef char int8_t;
    typedef short int16_t;
    typedef int int32_t;
    typedef long long int64_t;
    typedef unsigned char uint8_t;
    typedef unsigned short uint16_t;
    typedef unsigned int uint32_t;
    typedef unsigned long long uint64_t;
};

#include <LinuxPlatform.h>
#include <WindowsPlatform.h>

///
/// GGEZ_API is used to import or export from a DLL (or shared object)
///
/// When set on a class, the entire class is imported/exported; when set on a
/// function or variable, that function or variable is imported/exported.
///
/// Examples:
///
/// class GGEZ_API SomeClass
///
/// GGEZ_API void SomeFunc();
///
#if defined(GGEZ_BUILD_SHARED)
#define GGEZ_API GGEZ_EXPORT
#else // defined(GGEZ_BUILD_SHARED)
#define GGEZ_API GGEZ_IMPORT
#endif // defined(GGEZ_BUILD_SHARED)

///
/// Use for string literals to convert it to the platform type.
///
/// Example:
///
/// GGEZ_Str("Hello there");
///
#define GGEZ_Str(text) GGEZ_Plat_Str(text)

typedef PlatformTypes::int8_t int8_t;
typedef PlatformTypes::int16_t int16_t;
typedef PlatformTypes::int32_t int32_t;
typedef PlatformTypes::int64_t int64_t;
typedef PlatformTypes::uint8_t uint8_t;
typedef PlatformTypes::uint16_t uint16_t;
typedef PlatformTypes::uint32_t uint32_t;
typedef PlatformTypes::uint64_t uint64_t;
typedef PlatformTypes::char_t char_t;
typedef PlatformTypes::size_t size_t;
typedef PlatformTypes::ptrdiff_t ptrdiff_t;

} // namespace ggez

#endif // !defined(ENGINE_PLATFORM)

