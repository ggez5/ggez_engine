#if !defined(RUNNABLE_H)
#define RUNNABLE_H 1
#include <Platform.h>

namespace ggez
{

class Runnable
{
public:
    virtual ~Runnable() {}

    virtual void Run() = 0;
};

} // namespace ggez

#endif // !defined(RUNNABLE_H)
