#if !defined(WINDOWS_PLATFORM)
#define WINDOWS_PLATFORM 1

#if defined(_WIN32) || defined(WIN32)
#if !defined(_UNICODE)
#define _UNICODE 1
#endif // !defined(_UNICODE)
#if !defined(UNICODE)
#define UNICODE 1
#endif // !defined(_UNICODE)
#define GGEZ_IMPORT __declspec(dllimport)
#define GGEZ_EXPORT __declspec(dllexport)
typedef struct WindowsTypes : public GenericTypes
{
    typedef wchar_t char_t;
    typedef __int8 int8_t;
    typedef __int16 int16_t;
    typedef __int32 int32_t;
    typedef __int64 int64_t;
    typedef unsigned __int8 uint8_t;
    typedef unsigned __int16 uint16_t;
    typedef unsigned __int32 uint32_t;
    typedef unsigned __int64 uint64_t;

    template<typename LargeType, typename SmallType, unsigned __int64 size = sizeof(void*)>
    struct SizeType
    {};
    template<typename LargeType, typename SmallType>
    struct SizeType<LargeType, SmallType, 8>
    {
        typedef LargeType Type;
    };
    template<typename LargeType, typename SmallType>
    struct SizeType<LargeType, SmallType, 4>
    {
        typedef SmallType Type;
    };
    template<typename LargeType, typename SmallType>
    using SizeType_t = typename SizeType<LargeType, SmallType>::Type;

    typedef SizeType_t<uint64_t, uint32_t> size_t;
    typedef SizeType_t<int64_t, int32_t> ptrdiff_t;
} PlatformTypes;
#define GGEZ_Plat_Str(text) GGEZ_Join(L, text)
#endif // defined(_WIN32) || defined(WIN32)


#endif // !defined(WINDOWS_PLATFORM)

