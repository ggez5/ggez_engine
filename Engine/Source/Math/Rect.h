#if !defined(RECT)
#define RECT 1

#include <Platform.h>

namespace ggez
{
    struct Recti
    {
        Recti(int32_t xPos = 0, int32_t yPos = 0, int32_t width = 0, int32_t height = 0)
        {
            X = xPos;
            Y = yPos;
            Width = width;
            Height = height;
        }
        
        int32_t X;       //Top Left X Position of the rectangle
        int32_t Y;       //Top Left Y position of the rectangle
        int32_t Width;   //Width of the rectangle
        int32_t Height;  //Height of the rectangle
    };
}


#endif // !defined(RECT)