#if !defined(CIRCULAR_QUEUE)
#define CIRCULAR_QUEUE 1
#include <Platform.h>

namespace ggez
{

template<typename InType, size_t inBufferCount>
class GGEZ_API CircularQueue
{
public:
    typedef InType ElementType;

    CircularQueue();
    CircularQueue(const CircularQueue& other);
    CircularQueue(CircularQueue&& other) noexcept;
    CircularQueue& operator=(const CircularQueue& other);
    CircularQueue& operator=(CircularQueue&& other) noexcept;

    ~CircularQueue();

    bool HasItems() const;
    bool Enqueue(ElementType&& element);
    ElementType& GetFirst();
    void Pop();

private:
    CircularQueue& Assign(const CircularQueue& other);
    CircularQueue& Assign(CircularQueue&& other) noexcept;

    size_t m_elementCount{0};
    size_t m_currentIndex{0};
    size_t m_firstItem{0};
    ElementType m_queue[inBufferCount];
};

#include <CircularQueue.inl>

} // namespace ggez

#endif // !deifned(CIRCULAR_QUEUE)

