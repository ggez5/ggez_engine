#if !defined(WINDOWCONTEXT)
#define WINDOWCONTEXT 1

#include <Platform.h>
#include <Window/WindowSettings.h>
#include <glm.hpp>
#include <Math/GGEZMath.h>

namespace ggez
{
    /*
        WindowContext manages the lifetime of SDL_Window and provides an
        interface for manipulating/retrieving information the current window state
    */
    class GGEZ_API WindowContext
    {
        public:
            WindowContext(const WindowContext&) = delete;
	    WindowContext& operator=(const WindowContext&) = delete;
            WindowContext(WindowContext&&) = delete;
            WindowContext& operator=(WindowContext&&) = delete;
            
            //Constructs and initializes the SDL Window object and opens a window (Must be called first)
            static bool InitializeWindowContext(const char * title, int16_t width, int16_t height, WindowSettings windowSettings);
            //Cleans up the SDL Window object (Must be called before terminating the application if initialized)
            static bool Terminate();

            //Sets the title in the titlebar of the window
            static void SetTitle(const char * windowTitle);
            //Sets the top left position of the window
            static void SetWindowPosition(glm::i32vec2 position);
            //Sets the size of the screen
            static void SetWindowSize(int16_t width, int16_t height);
            //Used to set the window type (fullscreen/windowed with/without borders)
            static void SetFullscreen(bool includeBorders);
            static void SetWindowed(bool includeBorders);
            //Maximize and minimize the window
            static void Maximize();
            static void Minimize();
            //Returns the current top left position of the window
            static glm::i32vec2 GetWindowPosition();
            //Returns the bounds of the monitor the window exists on
            static Recti GetDisplayBounds();
            //Returns the bounds of the window
            static Recti GetWindowBounds();
            //Returns the current window settings
            static const WindowSettings & GetWindowSettings();

        private:
            WindowContext(){}

            //Converts WindowSettings to SDL Screen mode flags
            static uint32_t ProcessSDLFlags();
    };
}

#endif // !defined(WINDOWCONTEXT)
