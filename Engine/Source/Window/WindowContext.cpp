#include <Window/WindowContext.h>
#include <SDL_video.h>
#include <SDL.h>
#include <iostream>

//Window settings that corresponds with SDL flags
static ggez::WindowSettings WindowSetting;
//Value corresponds with what monitor the window exists on
static int32_t CurrentDisplayIndex;
//Height and Width of the window
static int16_t WindowHeight;
static int16_t WindowWidth;
//SDL Window object
static SDL_Window * SDLWindow;

bool ggez::WindowContext::InitializeWindowContext(const char * title, int16_t width, int16_t height, WindowSettings windowSettings)
{
    if (SDLWindow)
    {
        return false;
    }
    WindowSetting = windowSettings;
    WindowWidth = width;
    WindowHeight = height;
    //TODO move this init somewhere else, so we can init video/audio at one core place
    int numDrivers = SDL_GetNumVideoDrivers();
    std::cout << "found " << numDrivers << " video drivers" << std::endl;
    for(int i = 0; i < numDrivers; i++)
    {
        //TODO Use log
    	std::cout << SDL_GetVideoDriver(i) << std::endl;
    }
    if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        //TODO Use log
    	std::cout << "SDL init (SDL_INIT_VIDEO) failed" << std::endl;
    	std::cout << SDL_GetError() << std::endl;
    }
    SDLWindow = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WindowWidth, WindowHeight, ProcessSDLFlags());
    if (SDLWindow == nullptr)
    {
        //TODO Use log
    	std::cout << "Window was not created" << std::endl;
    	std::cout << SDL_GetError() << std::endl;
    }
    else
    {
        //TODO Use log
    	std::cout << "Window was created" << std::endl;
    }
    CurrentDisplayIndex = SDL_GetWindowDisplayIndex(SDLWindow);
    //Listen for Window Events for WindowEventFilter callback
    //SDL_AddEventWatch(ggez::WindowContext::WindowEventFilter, SDLWindow);
    return true;
}

bool ggez::WindowContext::Terminate()
{
    if (SDLWindow == nullptr)
    {
        return false;
    }
    //Stop event listening for Window Events
    //SDL_DelEventWatch(ggez::WindowContext::WindowEventFilter, SDLWindow);
    SDL_DestroyWindow(SDLWindow);
    return true;
}

void ggez::WindowContext::SetTitle(const char * windowTitle)
{
    if (SDLWindow)
    {
        SDL_SetWindowTitle(SDLWindow, windowTitle);
    }
}

void ggez::WindowContext::SetWindowPosition(glm::i32vec2 position)
{
    if (SDLWindow)
    {
        SDL_SetWindowPosition(SDLWindow, position.x, position.y);
    }
}

void ggez::WindowContext::SetWindowSize(int16_t width, int16_t height)
{
    if(SDLWindow)
    {
        SDL_SetWindowSize(SDLWindow, width, height);
        WindowHeight = height;
        WindowWidth = width;
    }
}

void ggez::WindowContext::SetFullscreen(bool includeBorders)
{
    if (SDLWindow)
    {
        WindowSetting.Fullscreen = true;
        WindowSetting.Borderless = includeBorders;
        SDL_SetWindowFullscreen(SDLWindow, includeBorders ? SDL_WINDOW_FULLSCREEN_DESKTOP : SDL_WINDOW_FULLSCREEN);
    }
}

void ggez::WindowContext::SetWindowed(bool includeBorders)
{
    if(SDLWindow)
    {
        WindowSetting.Fullscreen = false;
        WindowSetting.Borderless = includeBorders;
        SDL_SetWindowFullscreen(SDLWindow, 0);
        SDL_SetWindowBordered(SDLWindow, includeBorders ? SDL_TRUE : SDL_FALSE);
    }
}

void ggez::WindowContext::Maximize()
{
    if (SDLWindow)
    {
        SDL_MaximizeWindow(SDLWindow);
        ggez::Recti displayRect = GetDisplayBounds();
        WindowWidth = displayRect.Width;
        WindowHeight = displayRect.Height;
    }
}

void ggez::WindowContext::Minimize()
{
    if (SDLWindow)
    {
        SDL_MinimizeWindow(SDLWindow);
        WindowWidth = 0;
        WindowHeight = 0;
    }
}

glm::i32vec2 ggez::WindowContext::GetWindowPosition()
{
    glm::i32vec2 position(0,0);

    if (SDLWindow)
    {
        SDL_GetWindowPosition(SDLWindow, &position.x, &position.y);
    }

    return position;
}

ggez::Recti ggez::WindowContext::GetDisplayBounds()
{
    Recti rect;

    SDL_Rect sdlRect;
    SDL_GetDisplayBounds(CurrentDisplayIndex, &sdlRect);
    rect.X = sdlRect.x;
    rect.Y = sdlRect.y;
    rect.Width = sdlRect.w;
    rect.Height = sdlRect.h;

    return rect;
}

ggez::Recti ggez::WindowContext::GetWindowBounds()
{
    Recti rect(0, 0, WindowWidth, WindowHeight);
    return rect;
}

const ggez::WindowSettings & ggez::WindowContext::GetWindowSettings()
{
    return WindowSetting;
}
/*
TODO This will be replaced with our own event bus system, so leaving this here to be later repurposed
     Since the logic will basically be the same
int32_t SDLCALL ggez::WindowContext::WindowEventFilter(void * data, SDL_Event * event)
{
    if(event->type == SDL_WINDOWEVENT)
    {
        //Do stuff/update information when a window event occurs
        switch(event->window.event)
        {
            case SDL_WINDOWEVENT_MOVED:
                //Update the display index in case the monitor the window resides on changed
                CurrentDisplayIndex = SDL_GetWindowDisplayIndex(SDLWindow);
                break;
            case SDL_WINDOWEVENT_RESIZED:
                WindowWidth = event->window.data1;
                WindowHeight = event->window.data2;
                break;
            case SDL_WINDOWEVENT_SIZE_CHANGED:
                WindowWidth = event->window.data1;
                WindowHeight = event->window.data2;
                break;
            case SDL_WINDOWEVENT_MINIMIZED:
                WindowWidth = 0;
                WindowHeight = 0;
                break;
            case SDL_WINDOWEVENT_MAXIMIZED:
			{
				ggez::Recti displayRect = GetDisplayBounds();
				WindowWidth = displayRect.Width;
				WindowHeight = displayRect.Height;
				break;
			}
			case SDL_WINDOWEVENT_CLOSE:
                Terminate();
                break;
            default:
                break;
        }
    }
    return 0;
}
*/
//If new settings are added to WindowsSettings, flags are converted here to be used with SDL's api
uint32_t ggez::WindowContext::ProcessSDLFlags()
{
    uint32_t flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
    if (WindowSetting.Fullscreen)
    {
        flags |= WindowSetting.Borderless ? SDL_WINDOW_FULLSCREEN_DESKTOP : SDL_WINDOW_FULLSCREEN;
    }
    else if (WindowSetting.Borderless) //Borderless Windowed
    {
        flags |= SDL_WINDOW_BORDERLESS;
    }
    
    if (WindowSetting.Resizable)
    {
        flags |= SDL_WINDOW_RESIZABLE;
    }

    return flags;
}
