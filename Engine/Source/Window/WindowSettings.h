#if !defined(WINDOWSETTINGS)
#define WINDOWSETTINGS 1
#include <Platform.h>

namespace ggez
{
    //Use with SDL Flags in WindowsContext ProcessSDLFlags()
    struct WindowSettings
    {
        bool Fullscreen {false};
        bool Borderless {false};
        bool Resizable {false};
        bool VSync {false};
    };
}

#endif // !defined(WINDOWSETTINGS)