#include <Platform.h>

static_assert(sizeof(ggez::int8_t) == 1, "ggez::int8_t is wrong size!");
static_assert(sizeof(ggez::int16_t) == 2, "ggez::int16_t is wrong size!");
static_assert(sizeof(ggez::int32_t) == 4, "ggez::int32_t is wrong size!");
static_assert(sizeof(ggez::int64_t) == 8, "ggez::int64_t is wrong size!");
static_assert(sizeof(ggez::uint8_t) == 1, "ggez::uint8_t is wrong size!");
static_assert(sizeof(ggez::uint16_t) == 2, "ggez::uint16_t is wrong size!");
static_assert(sizeof(ggez::uint32_t) == 4, "ggez::uint32_t is wrong size!");
static_assert(sizeof(ggez::uint64_t) == 8, "ggez::uint64_t is wrong size!");
