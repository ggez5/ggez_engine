#include <assert.h>

#include <utility>

template<typename InType, size_t inBufferCount>
CircularQueue<InType, inBufferCount>::CircularQueue()
{}

template<typename InType, size_t inBufferCount>
CircularQueue<InType, inBufferCount>::CircularQueue(const CircularQueue& other)
{
    Assign(other);
}

template<typename InType, size_t inBufferCount>
CircularQueue<InType, inBufferCount>::CircularQueue(CircularQueue&& other) noexcept
{
    Assign(other);
}

template<typename InType, size_t inBufferCount>
CircularQueue<InType, inBufferCount>& CircularQueue<InType, inBufferCount>::operator=(const CircularQueue& other)
{
    return Assign(other);
}

template<typename InType, size_t inBufferCount>
CircularQueue<InType, inBufferCount>& CircularQueue<InType, inBufferCount>::operator=(CircularQueue&& other) noexcept
{
    return Assign(std::move(other));
}

template<typename InType, size_t inBufferCount>
CircularQueue<InType, inBufferCount>::~CircularQueue()
{
    for (size_t index = 0; m_elementCount > index; ++index) {
        size_t val = m_firstItem + index;
        m_queue[val % inBufferCount].~ElementType();
    }
}

template<typename InType, size_t inBufferCount>
bool CircularQueue<InType, inBufferCount>::HasItems() const
{
    return 0 != m_elementCount;
}

template<typename InType, size_t inBufferCount>
bool CircularQueue<InType, inBufferCount>::Enqueue(ElementType&& element)
{
    if (m_elementCount >= inBufferCount) {
        return false;
    }

    size_t index = m_currentIndex++;
    m_queue[index % inBufferCount] = std::move(element);
    m_elementCount++;
    return true;
}

template<typename InType, size_t inBufferCount>
typename CircularQueue<InType, inBufferCount>::ElementType& CircularQueue<InType, inBufferCount>::GetFirst()
{
    assert(HasItems());
    return m_queue[m_firstItem];
}

template<typename InType, size_t inBufferCount>
void CircularQueue<InType, inBufferCount>::Pop()
{
    if (0 == m_elementCount) {
        return;
    }
    m_queue[m_firstItem % inBufferCount].~ElementType();
    m_firstItem++;
    m_elementCount--;
}

template<typename InType, size_t inBufferCount>
CircularQueue<InType, inBufferCount>& CircularQueue<InType, inBufferCount>::Assign(const CircularQueue& other)
{
    m_elementCount = other.m_elementCount;
    m_currentIndex = other.m_currentIndex;
    m_firstItem = 0;
    for (size_t index = 0; other.m_elementCount > index; ++index) {
        size_t val = other.m_firstItem + index;
        m_queue[index] = other.m_queue[(val % inBufferCount)];
    }
    return *this;
}

template<typename InType, size_t inBufferCount>
CircularQueue<InType, inBufferCount>& CircularQueue<InType, inBufferCount>::Assign(CircularQueue&& other) noexcept
{
    m_elementCount = std::move(other.m_elementCount);
    m_currentIndex = m_elementCount;
    m_firstItem = 0;
    for (size_t index = 0; other.m_elementCount > index; ++index) {
        size_t val = other.m_firstItem + index;
        m_queue[index] = std::move(other.m_queue[(val % inBufferCount)]);
    }

    other.m_elementCount = 0;
    other.m_currentIndex = 0;
    other.m_firstItem = 0;
    return *this;
}

