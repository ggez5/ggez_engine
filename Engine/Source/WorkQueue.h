#if !defined(WORK_QUEUE)
#define WORK_QUEUE 1
#include <Platform.h>

#include <Runnable.h>

namespace ggez
{

class GGEZ_API WorkQueue
{
public:
    static WorkQueue* Instance();
    static uint32_t GetThreadMax();
    static uint64_t GetThreadId();

    ///
    /// Add an item to the work queue. workItem is expected to have its lifetime
    /// maintained by the creator during the duration of its time in the queue
    /// and during the work duration.
    ///
    /// Behaviour is undefined if workItem's lifetime is shorter than its
    /// duration in the work queue.
    ///
    void AddWorkItem(Runnable* workItem);

private:
    WorkQueue();
    WorkQueue(const WorkQueue&) = delete;
    WorkQueue(WorkQueue&&) = delete;
    WorkQueue& operator=(const WorkQueue&) = delete;
    WorkQueue& operator=(WorkQueue&&) = delete;

    ~WorkQueue();
};

} // namespace ggez

#endif // !defined(WORK_QUEUE)
