#if !defined(LINUX_PLATFORM)
#define LINUX_PLATFORM 1

#if defined(__gnu_linux__) && !defined(__ANDROID__)
#define GGEZ_IMPORT __attribute__((visibility("default")))
#define GGEZ_EXPORT __attribute__((visibility("default")))
typedef struct LinuxTypes : public GenericTypes
{
    typedef char char_t;
    typedef __SIZE_TYPE__ size_t;
    typedef __PTRDIFF_TYPE__ ptrdiff_t;
} PlatformTypes;
#define GGEZ_Plat_Str(text) text
#endif // defined(__gnu_linux__) && !defined(__ANDROID__)

#endif // !defined(LINUX_PLATFORM)

