#include <WorkQueue.h>

#include <atomic>
#include <chrono>
#include <mutex>
#include <thread>
#include <vector>

#include <CircularQueue.h>
#include <Runnable.h>

namespace
{

static ggez::CircularQueue<ggez::Runnable*, 12> g_workItems;
static std::atomic_bool g_isRunning{true};
static std::atomic<ggez::uint64_t> g_threadIdHandler{0};
static thread_local ggez::uint64_t threadId{0};
static std::mutex g_workQueueLock;

static void ThreadRunner()
{
    threadId = ++g_threadIdHandler;
    for(;g_isRunning;) {
        ggez::Runnable* runnable = nullptr;
        {
            std::lock_guard<std::mutex> lock(g_workQueueLock);
            if (g_workItems.HasItems()) {
                runnable = g_workItems.GetFirst();
                if (nullptr != runnable) {
                    g_workItems.Pop();
                }
            }
        }
        if (nullptr == runnable) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
            continue;
        }
        runnable->Run();
    }
}

static void InitializeThreadRunners()
{
    // Subtract one from here to account for the main thread.
    uint32_t threadMax = ggez::WorkQueue::GetThreadMax();
    for (uint32_t index = 0; threadMax > index; ++index) {
        std::thread thread(::ThreadRunner);
        thread.detach();
    }
}

} // namespace

namespace ggez
{

WorkQueue* WorkQueue::Instance()
{
    static WorkQueue workQueue;
    return &workQueue;
}

WorkQueue::WorkQueue()
{
    InitializeThreadRunners();
}

WorkQueue::~WorkQueue()
{}

uint32_t WorkQueue::GetThreadMax()
{
    // -1 to account for main thread, since most uses will not
    // account for the main thread.
    return std::thread::hardware_concurrency() - 1;
}

uint64_t WorkQueue::GetThreadId()
{
    return threadId;
}

void WorkQueue::AddWorkItem(Runnable* workItem)
{
    if (nullptr == workItem) {
        return;
    }

    std::lock_guard<std::mutex> lock(g_workQueueLock);

    g_workItems.Enqueue(std::move(workItem));
}

} // namespace ggez

