#/usr/bin/env bash

DIR=$(cd "$(dirname ${BASH_SOURCE[0]})" && pwd)
ROOT=$(cd "$(dirname $DIR)" && pwd)
INTERMEDIATE=$ROOT/Intermediate
BUILD=$ROOT/Build

if [[ ! -d $INTERMEDIATE ]]; then
    mkdir -p $INTERMEDIATE
fi

cd $INTERMEDIATE

cmake $BUILD -DCMAKE_BUILD_TYPE=$1 -DCMAKE_INSTALL_PREFIX=$ROOT/Finality
make && make install

cd $DIR

